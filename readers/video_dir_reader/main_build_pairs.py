import os
import sys
import numpy as np
from simple_caching import NpyFS
from pathlib import Path
from media_processing_lib.video import tryReadVideo
from tqdm import trange, tqdm
from argparse import ArgumentParser
from functools import partial

def videoGetDescriptor(video):
	# verySmart(TM)
	descriptorFirst = "%s-%s-%s" % (video[0].mean(), np.median(video[0]), video[0].std())
	descriptorSecond = "%s-%s-%s" % (video[1].mean(), np.median(video[1]), video[1].std())
	videoDescriptor = "%d-%s-%s" % (len(video), descriptorFirst, descriptorSecond)
	return videoDescriptor

def validFlow(frameA, frameB):
	try:
		from run import doInference as flowFunc
	except Exception:
		sys.path.append("pytorch-liteflownet")
		from run import doInference as flowFunc

	minFlow, maxFlow = 1, 5
	flow = flowFunc(frameA, frameB)
	absFlowMean = np.abs(flow.mean())
	valid = (absFlowMean > minFlow) and (absFlowMean < maxFlow)
	return valid

def validAll(frameA, frameB):
	return True

def search(video, left, right, f):
	if left == right:
		return None

	# Early exit for validAll
	try:
		_ = f(None)
		return left, right - 1
	except Exception:
		pass

	firstLeft, lastRight = None, None
	frames = video[left : right]
	for i in range(left, right):
		valid = f(frames[i - left])
		if not valid:
			if not lastRight is None:
				break
			else:
				continue
		
		if firstLeft is None:
			firstLeft = i
		lastRight = i

	if not firstLeft is None:
		return firstLeft, lastRight + 1
	else:
		return None

class SfmLearnerPairsMaker:
	def __init__(self, datasetPath, method):
		self.datasetPath = datasetPath
		self.method = method
		self.videoList = [str(x) for x in Path(datasetPath).glob("*.mp4")]
		self.cache = NpyFS("%s/.cache" % (os.path.abspath(os.path.realpath(datasetPath))))
		assert len(self.videoList) > 0

	def doVideos(self):
		N = len(self.videoList)
		print("[SfmLearnerParisMaker::doVideos] Building pairs for %d videos from %s" % (N, self.datasetPath))
		pbar = trange(N)
		pairs = {}
		indexes = []
		for i in pbar:
			videoName = self.videoList[i].split("/")[-1]
			pbar.set_description("[%s]" % videoName)
			videoPairs = self.doVideo(self.videoList[i])
			pairs[videoName] = videoPairs
			videoIndexes = [(videoName, ix) for ix in range(len(videoPairs))]
			indexes.extend(videoIndexes)

		self.pairs = pairs
		self.indexes = indexes

	def printVideoStatistics(self, video):
		N = len(video)
		T = 1 # number of seconds left/right
		step = int(T * video.fps)
		# Do the first pass to get statistics of this video on 15 frames from the middle.
		# Just for orientation purposes
		statsMean, statsStd = np.zeros((2 * step, N)) - 100, np.zeros((2 * step, N)) - 100
		for i in trange(N // 2, N // 2 + 15):
			potential = []
			frame = video[i]
			startFrame, endFrame = int(max(0, i - step)), int(min(N, i + step))
			for j in range(startFrame, endFrame):
				flow = flowFunc(frame, video[j])
				absFlow = np.abs(flow)
				statsMean[j - startFrame][i] = absFlow.mean()
				statsStd[j - startFrame][i] = absFlow.std()
		
		for i in range(2 * step):
			means = statsMean[i][np.abs(statsMean[i] + 100) > 1e-3]
			stds = statsStd[i][np.abs(statsStd[i] + 100) > 1e-3]
			cnt = len(means)
			meanMedian = np.median(means)
			stdMedian = np.median(stds)
			print(i - step, meanMedian, stdMedian, cnt)

	def doVideo(self, videoPath):
		np.random.seed(42)
		video = tryReadVideo(videoPath, "pims")
		pairsKey = "%s/pairs.npy" % videoGetDescriptor(video)
		if self.cache.check(pairsKey):
			return self.cache.get(pairsKey)

		N = len(video)
		T = 1 # number of seconds left/right
		step = int(T * video.fps)
		validFn = {
			"validAll" : validAll,
			"validFlow" : validFlow
		}[self.method]

		result = []
		for i in trange(N):
			frame = video[i]
			fValid = partial(validFn, frameB=frame)

			firstValid, lastValid = int(max(0, i - step)), int(min(N, i + step))
			left = search(video, firstValid, i, fValid)
			right = search(video, i + 1, lastValid + 1, fValid)

			left = list(range(left[0], left[1])) if not left is None else []
			right = list(range(right[0], right[1])) if not right is None else []
			framePairs = [i] + left + right

			if len(framePairs) > 1:
				result.append(framePairs)
		self.cache.set(pairsKey, result)
		return result

def getArgs():
	parser = ArgumentParser()
	parser.add_argument("datasetPath")
	parser.add_argument("--method", default="validAll")
	args = parser.parse_args()
	return args

def main():
	args = getArgs()
	x = SfmLearnerPairsMaker(args.datasetPath, args.method)
	x.doVideos()

if __name__ == "__main__":
	main()