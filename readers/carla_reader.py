import h5py
import numpy as np
from neural_wrappers.readers import CarlaH5PathsReader
from media_processing_lib.video import computeIntrinsics
from typing import Dict

class SfmLearnerCarlaReader(CarlaH5PathsReader):
	def __init__(self, datasetPath:str, sequenceSize:int, hyperParameters:Dict):
		h5File = h5py.File(datasetPath, "r")["train"]
		dataBuckets = {"data" : ["rgb", "depth", "pose"]}
		deltas = [+1]
		assert sequenceSize == 1
		super().__init__(h5File, dataBuckets=dataBuckets, deltas=deltas, hyperParameters=hyperParameters)

	def __getitem__(self, key):
		item = super().__getitem__(key)[0]
		targetRgb = ((item["rgb"] - 0.5) * 2).transpose(0, 3, 1, 2)
		sourceRgbs = np.expand_dims((item["rgb(t+1)"] - 0.5) * 2, axis=1).transpose(0, 1, 4, 2, 3)
		intrinsics = computeIntrinsics(fieldOfView=84, height=targetRgb.shape[0], width=targetRgb.shape[1], skew=0)
		intrinsics = np.repeat(intrinsics[None], len(targetRgb), axis=0)

		result = {
			"sourceRgbs" : sourceRgbs, "targetRgb" : targetRgb, "intrinsics" : intrinsics
		}
		return result, result