import numpy as np
import matplotlib.pyplot as plt
from pathlib import Path
from neural_wrappers.callbacks import Callback
from neural_wrappers.utilities import minMaxPercentile
from matplotlib.cm import hot
from media_processing_lib.image import tryWriteImage, toImage

def Hot(x):
	x = hot(minMaxPercentile(x, 2, 95))[..., 0 : 3]
	return x

class RandomPlotEpoch(Callback):
	def __init__(self, iterationsPerEpoch):
		super().__init__()
		self.iterationsPerEpoch = iterationsPerEpoch

	def onEpochStart(self, **kwargs):
		self.currentEpoch = kwargs["epoch"]
		Path("samples/%d" % self.currentEpoch).mkdir(exist_ok=True, parents=True)

	def plotFn(self, results, data):
		MB = len(data["targetRgb"])
		# Use only 1st item of the sequence for now.
		sourceToTargetWarped = results["ySourceToTargetWarped"][:, 0]
		masks = results["warpMask"][:, 0]
		sourceRgbs = data["sourceRgbs"][:, 0]
		yDiffs = np.abs(data["targetRgb"] - sourceToTargetWarped).mean(axis=1)

		for j in range(MB):
			sourceRgb = toImage(sourceRgbs[j])
			targetRgb = toImage(data["targetRgb"][j])
			ySourceToTargetWarped = toImage(sourceToTargetWarped[j])
			yTargetDisp = toImage(Hot(results["yTargetDisp"][j, 0]))
			diff = toImage(Hot(yDiffs[j]))
			mask = toImage(masks[j])
			stack = np.concatenate([sourceRgb, targetRgb, ySourceToTargetWarped, yTargetDisp, diff, mask], axis=1)
			tryWriteImage(stack, "samples/%d/%d.png" % (self.currentEpoch, j))

	def onIterationEnd(self, results, labels, **kwargs):
		if kwargs["iteration"] == 0:
			self.plotFn(results, kwargs["data"])
