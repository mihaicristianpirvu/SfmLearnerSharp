from neural_wrappers.pytorch import NWModule
from overrides import overrides
from utils import inverseWarp

from .sfm_learner import SfmLearner

def photometricLoss(y, t):
	seqSize = t["sourceRgbs"].shape[1]
	targetRgb = t["targetRgb"]
	targetDisparity = y["yTargetDisp"][:, 0]

	sequenceReconstructionLoss = 0
	sourceToTargetRgbs = t["sourceRgbs"] * 0
	masks = t["sourceRgbs"][:, :, 0] * 0
	for i in range(seqSize):
		sourceRgb = t["sourceRgbs"][:, i]
		targetToSourceRelativeTranslation = y["yRelativeTranslation"][:, i]
		targetToSourceRelativeRotation = y["yRelativeRotation"][:, i]

		# Apply inverse warp for this image/depth pair and fix nans
		sourceToTargetRgb, inBoundsMask = inverseWarp(sourceRgb, targetDisparity, \
			targetToSourceRelativeTranslation, targetToSourceRelativeRotation, t["intrinsics"])
		sourceToTargetRgbs[:, i] = sourceToTargetRgb.detach()
		masks[:, i] = inBoundsMask[:, 0].detach()

		# Apply pixel-wise L1 between the original target and the sourceToTarget warped images
		L1 = (targetRgb - sourceToTargetRgb).abs()
		reconstructionLoss = (L1 * inBoundsMask).mean()
		sequenceReconstructionLoss += reconstructionLoss

	# Mean the reconstruction loss over all the sequence
	sequenceReconstructionLoss /= seqSize
	return sequenceReconstructionLoss, sourceToTargetRgbs, masks

class SfmLearnerV1(SfmLearner):
	@overrides
	def networkAlgorithm(self, trInputs, trLabels, isTraining, isOptimizing):
		trForwardResult = self.forward(trInputs)

		trLoss, sourceToTargetRgbs, masks = photometricLoss(trForwardResult, trLabels)
		self.updateOptimizer(trLoss, isTraining, isOptimizing)

		trResults = {
			"sourceRgbs" : trLabels["sourceRgbs"], "targetRgb" : trLabels["targetRgb"], \
			"ySourceToTargetWarped" : sourceToTargetRgbs, "yTargetDisp" : trForwardResult["yTargetDisp"], \
			"warpMask" : masks, "photometricLoss" : trLoss.detach()
		}

		return trResults, trLoss

	@overrides
	def forward(self, x):
		yTargetDisp = self.dispNet(x["targetRgb"])
		yRelativePose = self.computeSequenceRelativePose(x["targetRgb"], x["sourceRgbs"])
		return {"yTargetDisp" : yTargetDisp, "yRelativeTranslation" : yRelativePose["yRelativeTranslation"], \
			"yRelativeRotation" : yRelativePose["yRelativeRotation"]}
