import numpy as np
import os
from argparse import ArgumentParser
from typing import Dict, Iterator, Optional, Union
from functools import partial
from overrides import overrides
from pathlib import Path
from neural_wrappers.readers import DatasetReader
from media_processing_lib.video import tryReadVideo

from .main_build_pairs import SfmLearnerPairsMaker

def mergeFn(items):
	x = {k : np.stack([item[k] for item in items], axis=0) for k in ["sourceRgbs", "targetRgb", "intrinsics"]}
	x["targetRgb"] = x["targetRgb"].transpose(0, 3, 1, 2)
	x["sourceRgbs"] = x["sourceRgbs"].transpose(0, 1, 4, 2, 3)
	return x, x

def framesGetter(dataset, index, pairsMaker, sequenceSize):
	# breakpoint()
	videoName, videoIndex = pairsMaker.indexes[index]
	pairs = pairsMaker.pairs[videoName][videoIndex]
	ixFrameA, ixFrameRest = pairs[0], pairs[1 :]

	np.random.seed(42)
	ixFrameB = np.random.choice(ixFrameRest, size=sequenceSize, replace=True)
	video = tryReadVideo(dataset[videoName], vidLib="pims")
	frameA, frameB = video[ixFrameA], video[ixFrameB]
	return videoName, frameA, frameB

class SfmLearnerVideoDirReader(DatasetReader):
	# @param[in] datasetPath A path to a directory full of mp4 video
	# @param[in] intrinsics A dict of name => K, or a K matrix (and a dict is created) or None (and default K is used)
	def __init__(self, datasetPath, intrinsics:Union[Dict[str, Optional[np.ndarray]], np.ndarray], \
		sequenceSize:int=1):
		self.datasetPath = datasetPath
		videoList = [os.path.abspath(str(x)) for x in Path(datasetPath).glob("*.mp4")]
		assert len(videoList) > 0
		assert sequenceSize >= 1
		self.sequenceSize = sequenceSize

		videoNames = [videoList[i].split("/")[-1] for i in range(len(videoList))]
		self.dataset = {videoNames[i] : videoList[i] for i in range(len(videoList))}
		if isinstance(intrinsics, np.ndarray):
			intrinsics = {videoNames[i] : intrinsics for i in range(len(videoList))}
		assert isinstance(intrinsics, Dict)
		for videoName in videoNames:
			assert videoName in intrinsics
		self.intrinsics = intrinsics

		self.pairsMaker = SfmLearnerPairsMaker(datasetPath, method="validAll")
		self.pairsMaker.doVideos()
		super().__init__(
			dataBuckets={"data" : ["frames"]},
			dimGetter={"frames" : partial(framesGetter, pairsMaker=self.pairsMaker, sequenceSize=self.sequenceSize)},
			dimTransform={}
		)

	@overrides
	def getDataset(self):
		return self.dataset

	@overrides
	def __len__(self):
		return len(self.pairsMaker.indexes)

	@overrides
	def __getitem__(self, key):
		item = super().__getitem__(key)["data"]["frames"]
		videoName, targetRgb, sourceRgbs = item
		intrinsics = self.intrinsics[videoName]

		sourceRgbs = (np.array(sourceRgbs).astype(np.float32) / 255 - 0.5) * 2
		targetRgb = (np.array(targetRgb).astype(np.float32) / 255 - 0.5) * 2

		result = {
			"sourceRgbs" : sourceRgbs, "targetRgb" : targetRgb, "intrinsics" : intrinsics
		}
		return result

	@overrides
	def __str__(self):
		Str = "[SfmLearner Video Dir Reader]"
		Str += "\n - Path: %s" % self.datasetPath
		Str += "\n - Sequence size: %d" % self.sequenceSize
		return Str