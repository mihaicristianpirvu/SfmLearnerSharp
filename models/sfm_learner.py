import torch as tr
import torch.nn as nn
import torch.nn.functional as F
from neural_wrappers.pytorch import device, NWModule, trModuleWrapper
from neural_wrappers.callbacks import PlotMetrics
from utils import inverseWarp

def photometricLoss(y, t):
	seqSize = t["sourceRgbs"].shape[1]
	targetRgb = t["targetRgb"]
	targetDisparity = y["yTargetDisp"][:, 0]

	sequenceReconstructionLoss = 0
	sourceToTargetRgbs = t["sourceRgbs"] * 0
	masks = t["sourceRgbs"][:, :, 0] * 0
	for i in range(seqSize):
		sourceRgb = t["sourceRgbs"][:, i]
		targetToSourceRelativeTranslation = y["yRelativeTranslation"][:, i]
		targetToSourceRelativeRotation = y["yRelativeRotation"][:, i]

		# Apply inverse warp for this image/depth pair and fix nans
		sourceToTargetRgb, inBoundsMask = inverseWarp(sourceRgb, targetDisparity, \
			targetToSourceRelativeTranslation, targetToSourceRelativeRotation, t["intrinsics"])
		sourceToTargetRgbs[:, i] = sourceToTargetRgb.detach()
		masks[:, i] = inBoundsMask[:, 0].detach()

		# Apply pixel-wise L1 between the original target and the sourceToTarget warped images
		L1 = (targetRgb - sourceToTargetRgb).abs()
		reconstructionLoss = (L1 * inBoundsMask).mean()
		sequenceReconstructionLoss += reconstructionLoss

	# Mean the reconstruction loss over all the sequence
	sequenceReconstructionLoss /= seqSize
	return sequenceReconstructionLoss, sourceToTargetRgbs, masks

class SfmLearner(NWModule):
	def __init__(self, dispNet, relativePoseNet, **k):
		super().__init__(hyperParameters=k)
		self.dispNet = trModuleWrapper(dispNet)
		self.relativePoseNet = relativePoseNet

		self.addMetrics({
			"photometricLoss" : lambda y, t, **k : y["photometricLoss"]
		})
		self.addCallback(PlotMetrics(["Loss", "photometricLoss"]))

	def computeSequenceRelativePose(self, targetRgb, sourceRgbs):
		MB, sequenceSize = sourceRgbs.shape[0], sourceRgbs.shape[1]
		resultTranslation = tr.empty(MB, sequenceSize, 3).to(device)
		resultRotation = tr.empty(MB, sequenceSize, 3).to(device)

		for i in range(sequenceSize):
			result = self.relativePoseNet(targetRgb, sourceRgbs[:, i])
			resultTranslation[:, i] = result["yRelativeTranslation"]
			resultRotation[:, i] = result["yRelativeRotation"]
		return {"yRelativeTranslation" : resultTranslation, "yRelativeRotation" : resultRotation}
